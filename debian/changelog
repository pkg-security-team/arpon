arpon (3.0-ng+dfsg1-5) unstable; urgency=medium

  * Team upload.
  * debian/control:
      - Bump Standards-Version to 4.6.2 - no changes needed.
      - Drop Depends on lsb-base because sysvinit-utils is essential.
  * debian/copyright: full update.
  * debian/upstream/metadata: update metadata.

 -- Carlos Henrique Lima Melara <charlesmelara@riseup.net>  Mon, 04 Dec 2023 19:49:35 -0300

arpon (3.0-ng+dfsg1-4.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Remove 1 maintscript entries from 1 files.
  * Update standards version to 4.6.0, no changes needed.

  [ Helmut Grohne ]
  * Install systemd units only once. (Closes: #1054190)

 -- Chris Hofstaedtler <zeha@debian.org>  Sat, 25 Nov 2023 14:19:08 +0100

arpon (3.0-ng+dfsg1-4) unstable; urgency=medium

  [ Samuel Henrique ]
  * Team upload.
  * Add salsa-ci.yml.
  * d/control: Add Pre-Depends field as per lintian
    skip-systemd-native-flag-missing-pre-depends

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Replace /var/run with /run for the Service PIDFile.
  * Set upstream metadata fields: Repository.

  [ Francisco Vilmar Cardoso Ruviaro ]
  * Bump DH to 13.
  * Bump Standards-Version to 4.5.1.
  * Add Rules-Requires-Root: no.
  * Switch to https url in debian/watch.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Mon, 01 Feb 2021 18:41:17 +0000

arpon (3.0-ng+dfsg1-3) unstable; urgency=medium

  * Create the log directory when configuring the package.
  * Update the package description.

 -- Lukas Schwaighofer <lukas@schwaighofer.name>  Wed, 10 Oct 2018 22:34:36 +0200

arpon (3.0-ng+dfsg1-2) unstable; urgency=medium

  * Team Upload.

  [ Lukas Schwaighofer ]
  * debian/control: Limit package to linux architectures.

  [ Raphaël Hertzog ]
  * Update team maintainer address to Debian Security Tools
    <team+pkg-security@tracker.debian.org>
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org

  [ Manas Kashyap ]
  * standard version updated
  * debhelper bumped

 -- Manas Kashyap <manaskashyaptech@gmail.com>  Sun, 26 Aug 2018 15:17:21 +0000

arpon (3.0-ng+dfsg1-1) unstable; urgency=low

  * Maintain arpon in pkg-security (Closes: #866832).
  * Imported Upstream version 3.0-ng.
    - Repacked without doc which contains the linkedin logo and pre-compiled
      PDFs without the source, violating the DFSG.
    - Drop existing patches, as the new version is a rewrite.
    - The libdumbnet library is now properly detected without using
      lsb-release (Closes: #783536).
    - The permissions of the log file are now 0640 and owned by root:root, the
      file is no longer world readable (Closes: #614518).
    - Stderr is properly redirected to /dev/null (Closes: #614027).
  * Update README.Debian: Mention missing documentation due to repack.
  * Update debian/copyright:
    - Convert to machine readable format.
    - Update for new version.
    - Add Files-Excluded for automatic uscan repacking without the doc dir.
  * Update debian/watch to deal with dfsg version suffix.
  * Package using git-buildpackage:
    - Add Vcs-{Git,Browser} to debian/control.
    - Add debian/gbp.conf.
  * Update to debhelper compatibility level 10, cleanup of debian/rules.
  * Drop Build-Depends on lsb-release.
  * Introduce a patch to fix gcc-7 compile errors.
  * Introduce a patch to adjust the installed files and paths.
  * Cleanup of control files:
    - Remove dirs, no longer needed.
    - Cleanup docs.
    - Prefix control files with packagename where appropriate.
  * Fix spelling errors in man page.
  * Add patch to remove unwanted build options.
  * Remove the pre-3.0 configuration file /etc/arpon.sarpi.
    - Explain the change in debian/NEWS.
  * Remove the /var/log/arpon folder in postrm (not just the contents).
  * Simplify and adjust init script for the new version:
    - Allow it to start multiple instances of arpon for different interfaces.
    - Support the status option.
  * Adjust logrotate configuration:
    - Only keep 4 weekly rotations.
    - Cleanup of unneeded options.
    - Updated the postrotate script for usage with systemd.
  * Provide a systemd service file.
  * Update /etc/default/arpon for new version and init script / service file.
  * Bump Standards-Version to 4.1.0.
  * Compile with NDEBUG, otherwise the log is flooded with debug messages that
    cannot be disabled.

 -- Lukas Schwaighofer <lukas@schwaighofer.name>  Thu, 07 Sep 2017 20:26:55 +0200

arpon (2.7.2-1) unstable; urgency=low

  * [863b18a] Imported Upstream version 2.7.2
  * [8cbe615] Switch to debhelper compat 9
  * [9572355] Switch to Standards-Version 3.9.6, no changes needed
  * [d2a2530] Authors file was renamed to AUTHOR, renamed it in debian/docs
  * [4b478b9] Updated patches and correct /etc/arpon.sarpi install path
  * [f4a52c7] Rewrote debian/rules

 -- Giuseppe Iuculano <iuculano@debian.org>  Sun, 08 Feb 2015 13:21:12 +0100

arpon (2.0-2) unstable; urgency=low

  * [9ce1735] Fix the default configuration /etc/default/arpon
    (Closes: #592310) - thanks to Luca Bruno
  * [96a4ebf] Uncomment examples in /etc/arpon.sarpi

 -- Giuseppe Iuculano <iuculano@debian.org>  Thu, 19 Aug 2010 19:18:09 +0200

arpon (2.0-1) unstable; urgency=low

  * [26180e8] Switch to quilt
  * [3d1be31] Imported Upstream version 2.0
  * [49351eb] Switch to dpkg-source 3.0 (quilt) format
  * [aeb7386] Bump to debhelper 7 compatibility
  * [e3882e9] added cmake, lsb-release in Build-Depends
  * [b598014] Use new cmake build system
  * [6a7cc92] Use dh_prep
  * [670ccab] updated copyright file
  * [fba9537] Install binary in usr/sbin
  * [f197a87] debian/init.d: Added $remote_fs in Required-Start and
    Required-Stop
  * [bcd8d8c] updated copyright file
  * [6bc5b28] Bump to standard-version 3.9.1, no changes needed
  * [e14a181] Updated my email address

 -- Giuseppe Iuculano <iuculano@debian.org>  Wed, 04 Aug 2010 13:29:45 +0200

arpon (1.90-1) unstable; urgency=low

  * New upstream release
  * debian/rules: use new make debian target
  * debian/docs: Added AUTHORS
  * Added debian/patches/02_fix_hypen.dpatch to fix hyphen-used-as-minus-sign

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Fri, 17 Oct 2008 20:01:06 +0200

arpon (1.50-1) unstable; urgency=low

  * Initial release (Closes: #492922)

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Tue, 19 Aug 2008 14:11:26 +0200
